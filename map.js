function map(elements,cb){
    let newArray=[]
    for (let val of elements){
       newArray.push(cb(val));
    }
    return newArray;
}
export {map}
