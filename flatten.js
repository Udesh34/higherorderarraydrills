function flatten(elements){
    let res=[];
    let x=[]
    for (let i=0;i<elements.length;i++){
        if (typeof(elements[i])==='string'){
            res.push(elements[i]);
        }
        else if (elements[i].length==undefined){
            res.push(elements[i]);

        }
        else{
            x=flatten(elements[i]);
            res=res.concat(x);
        }
    }
    return res;
}
export {flatten}
