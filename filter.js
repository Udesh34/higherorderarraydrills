function filter(elements,cb){
    const res=[];
    for(let i=0;i<elements.length;i++){
        if (cb(elements[i])){
            res.push(elements[i]);
        }
    }
    return res;
}
export {filter};
